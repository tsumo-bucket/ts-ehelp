@extends('layouts.admin')

@section('breadcrumbs')
<ol class="breadcrumb">
  <li><a href="{{route('adminDashboard')}}">Dashboard</a></li>
  <li><a href="{{route('adminUserRoles')}}">User Roles</a></li>
  <li class="active">Edit</li>
</ol>
@stop

@section('content')
<div class="col-lg-12">
  <div class="widget">
    <div class="header">
      <div>
        <i class="fa fa-file"></i> Form
      </div>
    </div>
  </div>
  {!! Form::model($data, ['route'=>['adminUserRolesUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit row']) !!}
  @include('admin.user_roles.form')
  {!! Form::close() !!}
</div>
@stop