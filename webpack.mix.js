const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// create global css imports
// create admin css imports
// this gets imported in app and admin
mix.sass('resources/sass/admin-import.scss', 'public/css');
mix.sass('resources/sass/admin.scss', 'public/css');
mix.sass('resources/sass/app-import.scss', 'public/css');
mix.sass('resources/sass/app.scss', 'public/css');

// ALL.JS -- global scripts`
const all = [
  // LIBRARIES FROM NODE MODULES
  'node_modules/jquery/dist/jquery.min.js',
  // 'node_modules/parselyjs/dist/parsley.min.js',
  'node_modules/popper.js/dist/umd/popper.min.js',
  'node_modules/bootstrap/dist/js/bootstrap.min.js',
  'node_modules/handlebars/dist/handlebars.min.js',
  'node_modules/underscore/underscore-min.js',

  // LIBRARIES FROM RESOURCES
  'resources/assets/js/sumo-plugins/sumo-app-asset-helper.js',
  'resources/assets/js/unload-warning.min.js',
  'resources/assets/js/sweetalert.min.js',
  ];

mix.scripts(all, 'public/js/all.js');


// APP.JS -- front end scripts
mix.scripts([
  'resources/assets/js/app.js'
  ], 'public/js/app.js');


// ADMIN.JS -- backend scripts
mix.scripts([... all,
  // LIBRARIES FROM NODE MODULES
  'node_modules/select2/dist/js/select2.full.min.js',

  // LIBRARIES FROM RESOURCES
  'resources/assets/js/jquery.dataTables.min.js',
  'resources/assets/js/jquery.sumo-datepicker.min.js',
  'resources/assets/js/dataTables.bootstrap.min.js',
  'resources/assets/js/parsley.min.js',
  'resources/assets/js/jquery-ui.min.js',
  'resources/assets/js/bootstrap-notify.min.js',
  'resources/assets/js/Jcrop.min.js',
  'resources/assets/js/handlebars.js',
  'resources/assets/js/redactor.min.js',
  'resources/assets/js/bootstrap-toggle.min.js',
  'resources/assets/js/unload-warning.js',
  'resources/assets/js/sweetalert.min.js',
  'resources/assets/js/jquery.fancybox.min.js',
  'resources/assets/js/sumo-admin.js',
  'resources/assets/js/sumo-asset-manager.js'
  ], 'public/js/admin.js');
  
if (mix.inProduction()) {
  mix.version();
}

mix.browserSync('127.0.0.1:8000');
